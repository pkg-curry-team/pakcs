#!/bin/bash

# Copyright (C) 2018 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# - None of the names of the copyright holders and contributors may be
#   used to endorse or promote products derived from this software without
#   specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

set -e
set -x

LIBS_BASE="usr/lib/pakcs"
DOCS_BASE="usr/share/doc/pakcs"
DPKG_INST_BASE="debian/pakcs"

SRC_FILE="${DPKG_INST_BASE}/$1"
_relative_file_path="$(echo "${SRC_FILE}" | sed -e "s@${DPKG_INST_BASE}/${LIBS_BASE}/@@")"
DST_FILE="${DPKG_INST_BASE}/${DOCS_BASE}/${_relative_file_path}"

mkdir -p "$(dirname $DST_FILE)"
mv "$SRC_FILE" "$DST_FILE"
ln -rs "${DST_FILE}" "${SRC_FILE}"
